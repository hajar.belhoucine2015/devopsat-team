cd "${0%/*}"
SERVICE_NAME=front_admin
echo "Building $SERVICE_NAME "
npm install 
docker build -t $SERVICE_NAME:latest .
if [[ "$1" == -d || "$2" == -d ]]
then
echo "Deploying $SERVICE_NAME "
docker-compose up
fi
