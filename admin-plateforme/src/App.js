import './App.css';
import TopNav from "./components/TopNav";
import Routes from "./components/Routes";
import Footer from "./components/Footer";

function App() {
  return (
      <div className="app">
          <TopNav />
          <main id="content" className="p-5">
              <Routes />
          </main>
          <Footer />


    </div>
  );
}

export default App;
