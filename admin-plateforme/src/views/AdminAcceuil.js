import * as React from 'react'
import BarChart from "../components/BarChart";
import PieChart from "../components/PieChart";

function AdminAcceuil() {

    return (
        <div className={'row'}>
            <div className={'col-xl-8'}>
                <BarChart/>
            </div>
            <div className={'col-xl-4 pt-5'}>
                <h2>Categories</h2>
                <PieChart/>
            </div>

        </div>
    );

    
}

export default AdminAcceuil;