import React, { useState, useEffect } from "react";
import AddProduct from "../components/AddProduct";
import {Card, CardDeck, Col, Form} from 'react-bootstrap'
import CartProd from "../components/cartProd";
import '../assets/styles/filterProd.css'
import axios from "axios"



function AdminProducts() {
    const [stateCartProduct,setCartProductState] = useState([]);
    
    const [categories,setCategories] = useState([]);
    useEffect(() => {
       getProducts();
       getCategories()
      }, []);
      

      const getProducts = () => {
        axios
          .get("http://localhost:8087/listProduit")
          .then(data => {
           // console.log(data);
           setCartProductState(data.data);

          })
          .catch(err => alert(err));

          };
          const getCategories = () => {
            axios
              .get("http://127.0.0.1:8087/listCategorie")
              .then(data => {
               // console.log(data);
               setCategories(data.data);
    
              })
              .catch(err => alert(err));
    
              };

    return (
        <main>
            <div className="row  m-0">

                <div className="col-xl-8 mt-4">
                    <h1>Products</h1>
                </div>
                <div className="col-xl-4  mt-4 text-right">
                    <AddProduct/>
                </div>


            </div>
            <div className="row  mt-4 pt-5">
                <div className="col-xl-3 ">
                    <Card>
                        <Card.Title>
                            <h4 className={'ml-2 mt-2 mb-4'}>Filter</h4>
                        </Card.Title>
                        <Card.Body>
                            <div>
                                <h6 className={'ml-2  mb-3'}>Category</h6>
                                <input type="radio" name="radio" id="radio1" checked="true"/>
                                <label className="radio" htmlFor="radio1">All Categories</label>
                                
                                {categories.map((cat, index) => (
                              <div>
                                <input type="radio" name="radio" id={index}></input>
                                <label htmlFor={index}> {cat.nom_categorie}</label>
                              </div>
                                ))}
                                
                            </div>
                            <div>
                                <h6 className={'ml-2 mt-4 mb-3'}>Price</h6>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="formGridUP">
                                        <Form.Label>Min</Form.Label>
                                        <Form.Control type="number" min={0}  placeholder="Enter min" />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="formGridReduction">
                                        <Form.Label>Max</Form.Label>
                                        <Form.Control type="number" min={0}  placeholder="Enter max" />
                                    </Form.Group>
                                    <button className={'btn-primary'} > >></button>
                                </Form.Row>

                            </div>
                            <div>
                                <h6 className={'ml-2 mt-4 mb-3'}>Reduction</h6>
                                <input type="radio" name="radio1" id="radio11" checked="true"/><label className="radio" htmlFor="radio11">Less then 10%</label>
                                <input type="radio" name="radio1" id="radio21"/><label htmlFor="radio21">10% - 30%</label>
                                <input type="radio" name="radio1" id="radio31"/> <label htmlFor="radio31">30% - 50%</label>
                                <input type="radio" name="radio1" id="radio41"/> <label htmlFor="radio41">More then 50%</label>
                            </div>
                            <div>
                                <h6 className={'ml-2 mt-4 mb-3'}>Date Added</h6>
                                <input type="radio" name="radio2" id="radio12" checked="true"/><label className="radio" htmlFor="radio12">Any Date</label>
                                <input type="radio" name="radio2" id="radio22"/><label htmlFor="radio22">In The Last Day</label>
                                <input type="radio" name="radio2" id="radio32"/> <label htmlFor="radio32">In The Last Week</label>
                                <input type="radio" name="radio2" id="radio42"/> <label htmlFor="radio42">In The Last Month</label>
                                <input type="radio" name="radio2" id="radio52"/> <label htmlFor="radio52">In The Last Year</label>
                            </div>
                            <div>
                                <h6 className={'ml-2 mt-4 mb-3'}>Rating</h6>

                            <div className="row">

                                <div className="col-xl-8 m-auto d-flex ">
                                    <div className="ml-4 star-rating mb-3 ">
                                        <input type="radio" id="5-stars" name="rating" value="5"/>
                                        <label htmlFor="5-stars" className="star">&#9733;</label>
                                        <input type="radio" id="4-stars" name="rating" value="4"/>
                                        <label htmlFor="4-stars" className="star">&#9733;</label>
                                        <input type="radio" id="3-stars" name="rating" value="3"/>
                                        <label htmlFor="3-stars" className="star">&#9733;</label>
                                        <input type="radio" id="2-stars" name="rating" value="2"/>
                                        <label htmlFor="2-stars" className="star">&#9733;</label>
                                        <input type="radio" id="1-star" name="rating" value="1"/>
                                        <label htmlFor="1-star" className="star">&#9733;</label>
                                    </div>

                                </div>
                            </div>
                            </div>
                        </Card.Body>
                    </Card>
                </div>


                <div className="col-xl-9 m-auto">
                    <div className={'row'}>
                    {stateCartProduct.map(p => (
                        <CartProd p={p}/>
                       
                    ))}
                       
                    </div>
                </div>
            </div>
        </main>
    );


}

export default AdminProducts;