import React,{useEffect, useState} from 'react'
import '../assets/styles/table.css'
import Paginate from "../components/paginate";
import {Col, Form, Row} from "react-bootstrap";
function AdminHistory() {
    const [filter_all, setFilterAll] = useState("");
    const [historiques,setHistoriques]=useState( [
        {
            id: 1,
            date: '08/12/2020',
            client_name: 'inas',
            product: 'product 1',
            qte: 3,
            price: 123.45,
            valide: 'oui'

        },
        {
            id: 2,
            date: '08/12/2020',
            client_name: 'hajar',
            product: 'product 2',
            qte: 3,
            price: 123.45,
            valide: 'oui'

        },
        {
            id: 3,
            date: '08/12/2020',
            client_name: 'nouhaila',
            product: 'product 3',
            qte: 3,
            price: 123.45,
            valide: 'oui'

        },
        {
            id: 4,
            date: '08/12/2020',
            client_name: 'atlas',
            product: 'product 4',
            qte: 3,
            price: 123.45,
            valide: 'oui'

        }

    ]);
  
   const all_historiques =[
    {
        id: 1,
        date: '08/12/2020',
        client_name: 'inas',
        product: 'product 1',
        qte: 3,
        price: 123.45,
        valide: 'oui'

    },
    {
        id: 2,
        date: '08/12/2020',
        client_name: 'hajar',
        product: 'product 2',
        qte: 3,
        price: 123.45,
        valide: 'oui'

    },
    {
        id: 3,
        date: '08/12/2020',
        client_name: 'nouhaila',
        product: 'product 3',
        qte: 3,
        price: 123.45,
        valide: 'oui'

    },
    {
        id: 4,
        date: '08/12/2020',
        client_name: 'atlas',
        product: 'product 4',
        qte: 3,
        price: 123.45,
        valide: 'oui'

    }

];
    const [current_page, setCurrentPage] = useState(1);
    const [last_page_url] = useState(historiques.length);

    function search(filter_all){

       setHistoriques( all_historiques.filter( item =>{
            let date = item.date.toString();
            let client_name = item.client_name.toString().toLowerCase();
            let product = item.product.toString().toLowerCase();
            let qte = item.qte.toString();
            let price = item.price.toString();
            let searchTerm = filter_all.toLowerCase();

            return date.includes(searchTerm)
                ||     client_name.includes(searchTerm)
                ||     product.includes(searchTerm)
                ||     qte.includes(searchTerm)
                ||     price.includes(searchTerm)

        }));
       
    }
    useEffect(() => {
        search(filter_all);
    }, [filter_all]);

    function handleChangePage(page) {
        setCurrentPage(page)
    }


    return (
        <main>
            <div className="row  m-0">
                <div className="col-xl-4 mt-4">
                    <h1>History</h1>
                </div>
            </div>
            <div className="row  mt-5">
                <div className="col-xl-10 col-lg-10 col-sm-12  m-auto">
                    <div className="card ">
                        <div className="card-body">
                            <div className={'row'}>
                                <div className={'col-xl-4 text-white'}>
                                    <Form.Group as={Row} controlId="formHorizontalName">
                                        <Form.Label column xl={3}>
                                            Recherche:
                                        </Form.Label>
                                        <Col xl={9}>
                                            <Form.Control type="search" placeholder="Recherche .."
                                                          value={filter_all}
                                                          onChange={event => setFilterAll(event.target.value)}/>
                                        </Col>
                                    </Form.Group>
                                </div>
                            </div>

                            <table className={"tab table b-table text-center "}>
                                <thead>
                                <tr>
                                    <th scope={'col'}>#</th>
                                    <th scope={'col'}>Date</th>
                                    <th scope={'col'}>Client Name</th>
                                    <th scope={'col'}>Product Name</th>
                                    <th scope={'col'}>Qte</th>
                                    <th scope={'col'}>Price</th>
                                    <th scope={'col'}>Valide</th>
                                </tr>
                                </thead>
                                <tbody>
                                {historiques.map((item, index) => (
                                    <tr>
                                        <td data-label={'#'} key={index}>{item.id}</td>
                                        <td data-label={'Date'}>{item.date}</td>
                                        <td data-label={'Client Name'}>{item.client_name}</td>
                                        <td data-label={'Product Name'}>{item.product}</td>
                                        <td data-label={'Qte'}>{item.qte}</td>
                                        <td data-label={'Price'}>{item.price} DH</td>
                                        <td data-label={'Valide'}>{item.valide}</td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>

                        </div>
                        <div className="row mt-3 mb-3">
                            <div className="col">
                                <div className="text-center">
                      <Paginate current_page={current_page} last_page_url={last_page_url} changePage={handleChangePage}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )


}

export default AdminHistory;