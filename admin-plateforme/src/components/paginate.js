import * as React from 'react'
import '../assets/styles/pagination.css'

function Paginate({current_page,last_page_url,changePage}) {
    return (
        <div>
            <ul className="pagination">
                {current_page !== 1 && <li onClick={(event) => changePage(current_page-1,event)} ><a>«</a></li>}
                {(current_page !== 1 && current_page !== 2) && <li onClick={(event) =>changePage(1,event)} ><a>1</a></li>}
                {current_page !== 1 && <li onClick={(event) =>changePage(current_page-1,event)} ><a>{current_page-1}</a></li>}
                <li><a className={"active"}>{current_page}</a></li>
                {current_page !== last_page_url && <li onClick={(event) =>changePage(current_page+1,event)} ><a>{current_page+1}</a></li>}
                {(current_page !== last_page_url && current_page !== last_page_url-1) && <li onClick={(event) =>changePage(last_page_url,event)} ><a>{last_page_url}</a></li>}
                {current_page !== last_page_url  && <li onClick={(event) =>changePage(current_page+1,event)} ><a>»</a></li>}
            </ul>
</div>
)
;


}

export default Paginate;