import  React, {  useState } from 'react'
import {Modal,Button,Container} from 'react-bootstrap'
import PieChart from "./PieChart";

function InfoProduct(props)  {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                Show Info
            </Button>

            <Modal show={show} onHide={handleClose}  size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Info Article</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <div className={'row'}>
                            <div className={'col-xl-6'}>
                                <h2>{props.p.titre_produit}</h2>
                                <h5 className={'text-center mt-2'}>{props.p.prix_unitaire} DH</h5>
                                <p className={'mt-4'}>Ventes: 45</p>
                                <p className={'mt-4'}>stock : 100</p>

                            </div>
                            <div className={'col-xl-6'}>
                                <PieChart/>
                            </div>
                        </div>
                    </Container>
                </Modal.Body>
                <Modal.Footer>

                    <Button variant="primary" onClick={handleClose}>
                        Voir Les Transactions en Details
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );


}

export default InfoProduct;
