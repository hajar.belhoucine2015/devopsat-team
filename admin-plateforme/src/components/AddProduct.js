import  React, { useState } from 'react'
import {Modal,Button,Form,Col,Row} from 'react-bootstrap'
import axios from "axios"



function AddProduct() {



    const submit = e => {
        let titre_produit = e.target[0].value;
        let description = e.target[1].value;
        let prix_unitaire = e.target[2].value;
        let data = {
            titre_produit,
            description,
            prix_unitaire
        };
       console.log(data);
       postProduct(data)
    };

        const postProduct = data => {
            axios
              .post("http://localhost:8080/ajoutProduit", data)
              .then(d => {
                console.log(d);
  
              })
              .catch(err => alert(err));
          };



    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
               Add Product
            </Button>


            <Modal show={show} onHide={handleClose}  size="lg"  >
                <Modal.Header closeButton>
                    <Modal.Title>NEW PRODUCT</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  
                <Form
        onSubmit={e => {
          e.preventDefault();
          submit(e);
        }}
      >



<div className={'row'}>
                           <div className={'col-xl-6'}>
                               <Form.Group as={Row} >
                                   <Form.Label column xl={3}>
                                      Titre Produit
                                   </Form.Label>
                                   <Col xl={7}>
                                       <Form.Control type="text" placeholder="Product Name" />
                                   </Col>
                               </Form.Group>
                              
                               
                               
                               <Form.Group as={Row} >
                                   <Form.Label column xl={3}>
                                      Catégorie
                                   </Form.Label>
                                   <Col xl={7}>
                                   <Form.Control as="select" defaultValue="Choose...">
                                           <option>Cat1</option>
                                           <option>Cat2</option>
                                           <option>Cat3</option>
                                           <option>Cat4</option>
                                       </Form.Control>
                                   </Col>
                               </Form.Group>
                               <Form.Group as={Row} >
                                   <Form.Label column xl={3}>
                                      Quantité Stock
                                   </Form.Label>
                                   <Col xl={7}>
                                       <Form.Control type="number" min={0} placeholder="Qte In Stock" />
                                   </Col>
                               </Form.Group>
                               <Form.Group as={Col}>
            <Form.File
              className="position-relative"
              required
              name="file"
              label="Images Product"
              
             
              id="validationFormik107"
              feedbackTooltip
            />
          </Form.Group>

                           </div>


                           <div className={'col-xl-6'}>
                               <Form.Row>
                                   <Form.Group as={Col} >
                                       <Form.Label>Unit Price</Form.Label>
                                       <Form.Control type="number" min={0}  placeholder="Enter Unit Price" />
                                   </Form.Group>

                                   <Form.Group as={Col} >
                                       <Form.Label>Reduction</Form.Label>
                                       <Form.Control type="text" placeholder="Reduction" />
                                   </Form.Group>
                               </Form.Row>
                               <Form.Row>
                                   <Form.Group as={Col} >
                                       <Form.Label>Size</Form.Label>
                                       <Form.Control as="select" defaultValue="Choose...">
                                           <option>XS</option>
                                           <option>MD</option>
                                           <option>XL</option>
                                           <option>XXL</option>
                                       </Form.Control>
                                   </Form.Group>

                                   <Form.Group as={Col} >
                                       <Form.Label>Capacite</Form.Label>
                                       <Form.Control type="number" min={0}  placeholder="Enter Capacite" />
                                   </Form.Group>
                               </Form.Row>
                               <Form.Row>

                                   <Form.Group as={Col} >
                                       <Form.Label>Description</Form.Label>
                                       <Form.Control as="textarea" rows={3} placeholder="Enter Description" />
                                   </Form.Group>
                               </Form.Row>

                           </div>





      </div>

        <button type="submit" className="btn btn-primary btn-sm">
          Add
        </button>
        
      </Form>


                       
                    
                </Modal.Body>
               
            </Modal>
     
                                   

        </>
    );

    
}

export default AddProduct;