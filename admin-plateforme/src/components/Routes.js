import React from 'react';
import { BrowserRouter,Route, Switch } from 'react-router-dom';
import AdminHistory from "../views/AdminHistory";
import AdminAcceuil from "../views/AdminAcceuil";
import AdminProducts from "../views/AdminProducts";
import Login from "./Login"
import Register from "./Register"


class Routes extends React.Component {
  render() {
      return (
<BrowserRouter>
      <Switch>
        <Route path='/history' exact component={AdminHistory} />
        <Route path='/products' exact component={AdminProducts} />
        <Route path='/' exact component={AdminAcceuil} />
        <Route path='/login' exact component={Login} />
        <Route path='/register' exact component={Register} />
        {/*<Route path='/dashboard' component={DashboardPage} />*/}
        {/*<Route path='/profile' component={ProfilePage} />*/}
        {/*<Route path='/tables' component={TablesPage} />*/}
        {/*<Route path='/maps' component={MapsPage} />*/}
        {/*<Route path='/404' component={NotFoundPage} />*/}
      </Switch>
</BrowserRouter>
      );

  }
}

export default Routes;
