import * as React from 'react'

export class Footer extends React.Component {
    render() {
        return (
           <footer className={'footer  mt-4 '}>
               <div className="container-fluid border-top p-2 ">
               <div className={'row'}>
                   <div className={'col-sm-8 ml-5'}>Design & Develop By Devopsat</div>

               </div>
               </div>
           </footer>
        )
    }

    
}

export default Footer;