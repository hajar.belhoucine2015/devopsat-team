import React from "react";
import {Card} from 'react-bootstrap'
import InfoProduct from "./InfoProduct";


function CartProd(props) {



    return (
     
        <div className={'col-xl-4'}>
       
    
        

            <Card>
             <Card.Img variant="top" src="../assets/images/Babouches/1.jpg" height="250"/>
             <Card.Body>
                 <Card.Title>{props.p.titre_produit} <br/>
                  <span class="text-primary  mt-2">{props.p.prix_unitaire} DH</span>
                  </Card.Title>
                 <Card.Text>
                   {props.p.description}
                 </Card.Text>
        
                 <InfoProduct p={props.p}/>
             </Card.Body>
             </Card>

    
        
           
     
        </div>
    );

    
}

export default CartProd;