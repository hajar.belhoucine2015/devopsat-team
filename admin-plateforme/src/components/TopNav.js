import * as React from 'react'
import {Nav, Navbar} from "react-bootstrap";
import '../assets/styles/header.css'

export class TopNav extends React.Component {
    constructor() {
        super();
        this.state = {};
    }


    render() {

        return (

            <Navbar collapseOnSelect expand="lg" className={'header'} variant="dark">
                <Navbar.Brand href="#home" className={'title'}>E-SHOP</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link href="/" >Acceuil</Nav.Link>
                        <Nav.Link href="/products">Products</Nav.Link>
                        <Nav.Link href="/history">History</Nav.Link>
                        <Nav.Link href="#logout"><i className="fas ml-4 fa-sign-out-alt" /></Nav.Link>

                    </Nav>

                </Navbar.Collapse>
            </Navbar>
        )
    }

    
}

export default TopNav;