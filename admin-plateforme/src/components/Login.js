import React, { Component } from 'react';
import { Row ,Col, Card, Form, InputGroup, FormControl,Button} from 'react-bootstrap';


class Login extends Component {
    constructor(props){
        super(props);
        this.state = this.initialState;
    }

initialState = {

    email:'', password:''

};
resetLoginForm = () =>{
    this.setState( () => this.initialState);
    
};
CredentielChange = event => {
    this.setState({
        [event.target.name]:event.target.value

    });
};
    render() {
        const{email,password} = this.state
        return (
            <Row className="justify-content-md-center"> 
                <Col xs={5}>
                    <Card>
                        <Card.Header>
                           Login

                        </Card.Header>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group>
                                    <InputGroup.Prepend>
                                    <InputGroup.Text></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl required autoComplete="off" type="text" name="email" placeholder="Enter email" className="bg-dark text-white" value={email} onChange={this.CredentielChange} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group>
                                    <InputGroup.Prepend>
                                    <InputGroup.Text></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl  type="text"  name="password" placeholder="Enter password" value={password}  onChange={this.CredentielChange} />
                                </Form.Group>
                            </Form.Row>

                        </Card.Body>
                        <Card.Footer>
                            
                            <Button type="button" variant="success" disabled={this.state.password.length == 0 || this.state.email.length == 0}>Login</Button>
                            <Button type="button" variant="info" disabled={this.state.password.length == 0 && this.state.email.length == 0} onClick={this.resetLoginForm}>Reset</Button>

                        </Card.Footer>
                    </Card>
            

                    
                </Col>

            </Row>
        );
    }
}

export default Login;