import * as React from 'react'
import {Pie} from 'react-chartjs-2';
const data = {
    labels: [
        'Ventes',
        'Stock',
    ],
    datasets: [{
        data: [45, 100],
        backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
        ],
        hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
        ]
    }]
};

function PieChart() {

    return (
        <div>
            <Pie data={data} />
        </div>
    );

    
}

export default PieChart;