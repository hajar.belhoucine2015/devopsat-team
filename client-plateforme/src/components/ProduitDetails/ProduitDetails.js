import Footer from '../Footer/Footer';
import Menu from '../Header/Menu';

import image23 from '../../images/image23.jpg'
import image14 from '../../images/image14.jpg'
import image13 from '../../images/image13.jpg'

import image15 from '../../images/image15.jpg'


import React  from 'react';

import {Button, ButtonToolbar} from 'react-bootstrap';


 

import '../ProduitDetails/ProduitDetails.css';

function ProduitDetails(){
    return(


<div>

        
            
                <div class="col-md-4">
                    <img src={image23} width="100%" id="ProductImg"/>
                    <div class="small-imgs">
                        <img src={image14} width="100%" class="small-img"/>
                        <img src={image15} width="100%" class="small-img"/>
                        <img src={image13} width="100%" class="small-img"/>
                        
                    </div>                
                </div>
                <div class="col-md-6">
                    <h1 class="product-title">Barchouche Zwine </h1>
                   
                    <div class="price">
                        <span>380 DH</span>
                        <span>490 DH</span>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Color</h4>
                            <div class="colors">
                                <div class="black"></div>
                                <div class="red"></div>
                                <div class="white"></div>
                                <div class="yellow"></div>
                            </div>
                        </div>
                        <div class="col-md-4 sze">
                            <h5>Size</h5>
                            <select class="size custom-select">
                                <option>Selectionner la taille</option>
                                <option>39</option>
                                <option>40</option>
                                <option>41</option>
                                <option>42</option>
                            </select>
                        </div>
                        <div class="col-md-4 qty">
                            <h5>Quantity</h5>
                            <select class="quantity custom-select">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div id="product" class="product-inf">
                        <ul>
                          <li class="active"><a href="#Description">Product Description</a></li>
                         
                        </ul>
                        <div class="tabs-content">
                            <div id="Description">
                                <p>Zwine f lbass matnedmich ila cheritih 3la 7ssabi khoudih</p>
                            </div>
                          
                        </div>
                    </div>
                    <div class="buttons">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" class="custom-btn">Ajouter <i class="fas fa-angle-right"></i></a>
                            </div>
                            <div class="col-md-6">
                                <a href="#" class="custom-btn"> Favorite<i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
        
        
    
    

   
</div>
)
}

export default ProduitDetails;
