 import React , {useState, useEffect} from 'react'
 import '../Header/Menu.css'
 import { FaSearch } from "react-icons/fa";
import InputGroup from 'react-bootstrap/InputGroup'
import {Form,FormControl,} from "react-bootstrap";
import { RiShoppingBasket2Line,RiLoginCircleFill,RiLoginCircleLine  } from "react-icons/ri";

 import image10 from '../../images/image10.jpg';
 import image23 from '../../images/image23.jpg';
 import image15 from '../../images/image15.jpg';
 import image14 from '../../images/image14.jpg';
 import axios from "axios"


 
function Menu(){

    let subMenu;


    const [stateCategory,setCategoryState] = useState([]);
    useEffect(() => {
  
       console.log("hajatTest");
       getCategories();
      }, []);
  
      const getCategories = () => {
        axios
          .get("http://localhost:8087/listCategorie")
          .then(data => {
           // console.log(data);
           setCategoryState(data.data);
  
          })
          .catch(err => alert(err));
  
          };







    return(
        <header className="header">
            <div className="container">
                <div className="row v-center">
                    <div className="header-item item-left">
                        <div className="logo">
                            <a href="/">TRADYSHOP</a>
                        </div>
                    </div>
          
                    <div className="header-item item-center">
                        <div className="menu-overlay" onClick={toggleMenu}>
                        </div>
                        <nav className="menu">
                            <div className="mobile-menu-head">
                            <div className="go-back"  onClick={hideSubMenu} ><i class="fa fa-angle-left"></i></div>                               
                                <div className="current-menu-title"></div>
                                <div className="mobile-menu-close" onClick={toggleMenu}>&times;</div>
                            </div>
                            <ul className="menu-main"onClick={xii}>
                                <li>
                                    <a href="/">Acceuil</a>
                                </li>
                                <li className="menu-item-has-children">
                                    <a href="#">Nouveaux Produits <i className="fa fa-angle-down"></i></a>
                                    <div className="sub-menu xx mega-menu mega-menu-column-4">
                                        <div className="list-item text-center">
                                            <a href="#">
                                            <img      src={image23} alt="new Product" />
                                            <h4 className="title">Product 1</h4>
                                            </a>
                                        </div>
                                        <div className="list-item text-center">
                                            <a href="#">
                                            <img      src={image15} alt="new Product" />
                                            <h4 className="title">Product 2</h4>
                                            </a>
                                        </div>
                                        <div className="list-item text-center">
                                            <a href="#">
                                            <img src={image14} alt="new Product" />
                                            <h4 className="title">Product 3</h4>
                                            </a>
                                        </div>
                                        <div className="list-item text-center">
                                            <a href="#">
                                            <img src={image15} alt="new Product" />
                                            <h4 className="title">Product 4</h4>
                                           </a>
                                        </div>
                                    </div>
                                </li>
                                <li className="menu-item-has-children">
                                    <a href="/produits">Catégories<i className="fa fa-angle-down"></i></a>
                                    <div className="sub-menu xx mega-menu mega-menu-column-4">
                                     
                                    <div className="list-item">
                                            <h4 className="title">  {stateCategory.map((cat) => (
                              <div>
                             
                             {cat.id_categorie} : {cat.nom_categorie}
                          
                              </div>
                              
                                ))}</h4>
                                           
                                            <h4 className="title">Accesoires</h4>
                                            <ul>
                                                 <li><a href="#">Foulards</a></li>
                                                 <li><a href="#">Sacs</a></li>
                                                 <li><a href="#">Bijoux</a></li>
                                            </ul>
                                      </div>


                                

                                   
                                   
                                      <div className="list-item">
                                           <img src={image10} alt="shop" />
                                      </div>
                                    </div>
                                </li>
                                <li className="menu-item-has-children">
                                    <a href="#">Connexion<i className="fas fa-angle-down"></i></a>
                                    <div className="sub-menu  xx single-column-menu">
                                        <ul>
                                            <li><a href="/connexion">< RiLoginCircleLine/> Login</a></li>
                                            <li><a href="/register">< RiLoginCircleFill/> Register</a></li>
                                            
                                        </ul>
                                    </div>
                                </li>
                               
                            
                                
                            </ul>
                        </nav>
    
                    </div>

                    <div className="header-item item-right">
                    <div className="icon"><a href="/card">< RiShoppingBasket2Line size={25} style={{ fill: 'black' }}/></a></div>
 		
                           
        <Form inline>

  
<InputGroup>

  <FormControl type="text" placeholder=" Search" className="mr-sm-2" />
  <div className="search-icon"><FaSearch /></div>

</InputGroup>  

   


</Form>
                 
                        <div className="mobile-menu-trigger" onClick={toggleMenu}>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>





    )

    function toggleMenu(){
        document.querySelector(".menu").classList.toggle("active");
        document.querySelector(".menu-overlay").classList.toggle("active");
    }


    function  hideSubMenu(){  
        subMenu = document.querySelector(".xx");
  
        setTimeout(() =>{
           subMenu.classList.remove("active");	
        },300); 
        document.querySelector(".menu").querySelector(".current-menu-title").innerHTML="";
        document.querySelector(".menu").querySelector(".mobile-menu-head").classList.remove("active");
        
        document.querySelector(".menu").querySelector(".menu-main").classList.remove("active");
     }



 function showSubMenu(hasChildren){
    subMenu = hasChildren.querySelector(".sub-menu");
    subMenu.classList.add("active");
    subMenu.style.animation = "slideLeft 0.5s ease forwards";
    const menuTitle = hasChildren.querySelector("i").parentNode.childNodes[0].textContent;
    document.querySelector(".menu").querySelector(".current-menu-title").innerHTML=menuTitle;
    document.querySelector(".menu").querySelector(".mobile-menu-head").classList.add("active");
    
 }

 function xii(e){

    if(!document.querySelector(".menu").classList.contains("active")){
        return;
    }
  if(e.target.closest(".menu-item-has-children")){
       const hasChildren = e.target.closest(".menu-item-has-children");
     showSubMenu(hasChildren);
  }

 }


 /*   A revoir c'est quoi
 window.onresize = function(){
    if(this.innerWidth >991){
        if(document.querySelector(".menu").classList.contains("active")){
            toggleMenu();
        }

    }
}*/



}


    

export default Menu;







