import Carousel from 'react-bootstrap/Carousel'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import '../Home/Home.css'
import image1 from '../../images/image2.jpg';
import image2 from '../../images/image3.jpg';
import image6 from '../../images/image6.jpg';
import image7 from '../../images/image1.jpg';
import image8 from '../../images/image8.jpg';
import image9 from '../../images/image9.jpg';
import {AiFillGift,AiFillCrown} from "react-icons/ai";
import { MdEmail} from "react-icons/md";
import emailjs from 'emailjs-com';

function Home(){
  function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('service_ndczqgd', 'template_uizv0t9', e.target, 'user_1kmFZPIrXvgwMda17FCNG')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
  }
    return(

  <div className="Home-content">
        <div className="home-slide"> 
             <div className ="slide">
               <Carousel className="carousel">

                    <Carousel.Item interval={1000}>
                        <img className="d-block w-100"src={image1} alt="First slide" />
                        <Carousel.Caption className="pub1"> 
                            <h3>CAFTANT FAIT MAIN PAR LES MEILLEURS ARTISANTS DU MAROC</h3>
                            <h2>CAFTAN PRET A PORTER : FEMININ</h2>
                            <h1>RESPONSABLE ET ETHETHIQUE</h1>
                        </Carousel.Caption>
                    </Carousel.Item>

                    <Carousel.Item interval={500}>
                        <img className="d-block w-100" src={image2} alt="Third slide"/>
                        <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>

                    <Carousel.Item>
                        <img className="d-block w-100"  src={image9}  alt="Third slide"   />
                        <Carousel.Caption>
                          <h3>CHIC & MODERNE </h3>
                          <p>DECOUVREZ LA COLLECTION DES CAFTANS HOMMES </p>
                        </Carousel.Caption>
                    </Carousel.Item>

                </Carousel>
              </div>

            {/* End Home Slide */}
           </div>

      <div className="home-grid container">
        <div className="grid">
          <div className="grid-title">
                <h2><span class="fast-flicker"> BEST </span> SELLER <span class="flicker"> PRODUCTS</span><AiFillCrown/></h2>
          </div>
              <CardDeck className="allCards">
                <Card className="box">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                  <Card.Img variant="top" src={image7} />
                  <Card.Footer className="card-home">
                    <button >
                      <p>Shop Now</p>
                    </button>
                  </Card.Footer>
                </Card>

                <Card className="box"> 
                  <Card.Img variant="top" src={image6}/>
                  <Card.Footer>
                    <button className="text-muted">
                        <p>Shop Now</p>
                      </button>
                  </Card.Footer>
                </Card>

                <Card className="box">
                  <Card.Img variant="top" src={image8} />
                  <Card.Footer>
                    <button className="text-muted">
                      <p>Shop Now</p>
                    </button>
                  </Card.Footer>
                </Card>
                
              </CardDeck>
          </div>
      </div>

      <div className="home-announcement">
          <div className="annoucement-title">
                <h2> <AiFillGift />Idées Cadeaux <AiFillGift /></h2>
                <div className="announcement">
                      <a href="#">
                      <div className="image-btn"> 
                  <svg className="svage">
	                    <symbol id="s-text">
		                   <text text-anchor="middle" x="50%" y="60%">Shop Now</text>
	                    </symbol>

	                  <g class = "g-ants">
                      <use  xlinkHref="#s-text" class="text-copy"></use>
                      <use  xlinkHref="#s-text" class="text-copy"></use>
                      <use  xlinkHref="#s-text" class="text-copy"></use>
                      <use  xlinkHref="#s-text" class="text-copy"></use>
                      <use  xlinkHref="#s-text" class="text-copy"></use>
                    </g>
                  </svg>
                       </div>
                      </a>
              </div>  
         </div>
      {/* End Home Annoucement */}
      </div>

    
      <div className="home-newsletter">  
      <form className="contact-form" onSubmit={sendEmail}>
            <div className="wrapper">
	              <h1>Subscribe to our Newsletter</h1>
	              <p><MdEmail/> Receive updates instanly</p>
	          <div class="newsletter">
		            <input type="text" class="input" placeholder="Enter Your Email" />

                <input className="btn" type="submit" value="Subsribe" />
	          </div>
            </div>	
            </form>
      {/*  End Home Subscribe */}
      </div>

</div> 
    )
}
export default Home;
