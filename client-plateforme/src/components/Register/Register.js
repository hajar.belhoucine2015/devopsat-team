import React from 'react'
import {Form,Button,Col} from 'react-bootstrap'
import Footer from '../Footer/Footer';
import Menu from '../Header/Menu';
import '../Register/Register.css';


function Register(){
    return(
  <div>
    
        <Menu/>
                <div className="container">
                  <div className="description"></div>
                  <h1 className="purpose">Pourquoi s'inscrire</h1>
                  <h2>Créez un compte sur TRADYSHOP et bénéficiez de nos services personnalisés.</h2>
                  <p>Recevez des e-mails en exclusivité qui vont te permettre de</p>
                  <p>Etre la première à connaître nos dernières promos et offres.</p>
                  <p>Etre au courant des dernières notifications sur vos commandes.</p>

                  
                <div className="formulaire">  

        <Form>
              <Form.Row>
                
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" placeholder="Enter email" />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Label>Mot de Passe</Form.Label>
                  <Form.Control type="password" placeholder="Password" />
                </Form.Group>

              </Form.Row>

                <Form.Group controlId="formGridAddress1">
                  <Form.Label>Addresse</Form.Label>
                  <Form.Control placeholder="1234 Main St" />
                </Form.Group>

                <Form.Group controlId="formGridAddress2">
                   <Form.Label>Addresse 2</Form.Label>
                   <Form.Control placeholder="Apartment, studio, or floor" />
                </Form.Group>

            <Form.Row>

                <Form.Group as={Col} controlId="formGridCity">
                  <Form.Label>Ville</Form.Label>
                  <Form.Control />
                </Form.Group>

               <Form.Group as={Col} controlId="formGridState">
                  <Form.Label>Sexe</Form.Label>
                  <Form.Control as="select" defaultValue="Choose...">
                    <option>Choisir...</option>
                    <option>Femme</option>
                    <option>Homme</option>
                  </Form.Control>
               </Form.Group>

              <Form.Group as={Col} controlId="formGridZip">
                  <Form.Label>Telephone</Form.Label>
                  <Form.Control />
              </Form.Group>
              
            </Form.Row>



        </Form>



        <div className="submitbtn"> <Button className="submit"variant="primary" type="submit"> Submit   </Button></div>
          
            {/* End Formulaire */}
            </div>
        {/* End Container */}
        </div>

        <Footer/>
  </div>

    )
                    }

                    
export default Register;