import  React, { useState } from 'react'
import {Modal,Button,Form,Col,Row} from 'react-bootstrap'
import image1 from '../../images/image1.jpg'

import '../ProduitDetails/ProduitDetails.css';


function AddProduit(props) {


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>

<div class="card">
  <button  onClick={handleShow}>Details</button>
</div>

            
           
            <Modal show={show} onHide={handleClose}  size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>{props.p.titre_produit}</Modal.Title>
                </Modal.Header>

                <div>

        
            <div class="row row1">
                <div class="col-md-4">
                    <img src={image1} width="100%"  id="ProductImg"/>
                      
                </div>
                <div class="col-md-6">
                   
                   
                 
             
                    <div id="product" class="product-inf">
                        <ul>
                          <li class="active"><a href="#Description">Product Description</a></li>
                         
                        </ul>
                        <div class="tabs-content">
                            <div id="Description">
                            <p>
                {props.p.description}
</p>
                            </div>
                          
                        </div>
                    </div>
                    <div class="row mr-4" >
                    <div class="col-md-4 sze">
                        <h3 className="text-primary">Prix</h3>
                        <span>{props.p.prix_unitaire} </span>
                        </div>
                        
                        <div class="col-md-4 sze">
                            <h5>Taille</h5>
                            <select  value={props.p.designation.taille} class="size custom-select">
                                <option></option>
                                <option>s</option>
                                <option>M</option>
                                <option>L</option>
                                <option>XL</option>
                            </select>
                        </div>
                        <div class="col-md-4 qty">
                            <h5>Quantité</h5>
                            <select class="quantity custom-select">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="buttons">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#" class="custom-btn">Ajout Panier </a>
                            </div>
                            <div class="col-md-6">
                                <a href="#"  onClick={handleClose} class="custom-btn"> annuler</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
    
    

   
</div>

            
                

                
               
            </Modal>
        </>
    );

    
}

export default AddProduit;