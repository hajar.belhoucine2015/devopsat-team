import React from 'react'
import './Footer.css'
import { AiFillFacebook,AiFillTwitterSquare,AiFillYoutube,AiFillInstagram} from "react-icons/ai";

function Footer(){
    return(
      <div className="mai-footer pt-4 mt-3" id="footer">
        <div className="container">
          <div className="row">
            {/* Column1 */}
            <div className="col-md-3 col-sm-6">
              <h4>SHOP</h4>
              <ul className="list-unstyled">
                <li>Femme</li>
                <li>Homme</li>
                <li>Enfant</li>
                <li>Promotion</li>
              </ul>
            </div>
            {/* Column2 */}
            <div className="col-md-3 col-sm-6">
              <h4>INFORMATIONS</h4>
              <ul className="list-unstyled">
                <li>Qui Sommes-Nous?</li>
                <li>Affilié</li>
                <li>Blogger</li>
                <li>ENVIRONNEMENTALE</li>
              </ul>
            </div>
            {/* Column3 */}
            <div className="col-md-3 col-sm-6">
              <h4>CENTRE & D'AIDE</h4>
              <ul className="list-unstyled">
                <li>Livraison</li>
                <li>Commande</li>
                <li>Statut de Commande</li>
                <li>Guide des tailles</li>
              </ul>
            </div>
            {/* Column4 */}
            <div className="col-md-3 col-sm-6">
              <h4>CONTACT</h4>
              <ul className="list-unstyled">
            
                <li><AiFillFacebook/> <a>Facebook/Tradi_Shop</a></li>
                <li><AiFillInstagram/> <a>Instagram/Tradi_Shop</a></li>
                <li><AiFillTwitterSquare/> <a>Twitter/Tradi_Shop</a></li>
                <li><AiFillYoutube/> <a>Youtube/Tradi_Shop</a></li>
              </ul>
            </div> 
          </div>
         {/* Footer Bottom */}
         <div className="footer-bottom mt-4">
           <div className="text-xs-center">
             &copy; {new Date().getFullYear()} Designed By DevOpsat
           </div>

         </div>
        </div>
      </div>
       )
}
export default Footer;