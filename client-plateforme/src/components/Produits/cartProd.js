import React from "react";
import {Card} from 'react-bootstrap'
 import image2 from '../../images/image1.jpg';
 import AddProduit from "../Produit/AddProduit";




function CartProd(props) {



    return (
     
        <div className={'col-xl-4'}>
       
    
        
      
            <Card >
            <Card.Img variant="top" src={image2} />
             <Card.Body>
                 <Card.Title>{props.p.titre_produit}</Card.Title>
              
             </Card.Body>
            <div>      <AddProduit p={props.p}  /></div>
              
        
             </Card>

    
        
           
     
        </div>
    );

    
}

export default CartProd;