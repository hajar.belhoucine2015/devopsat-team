import React , {Component} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'

import DropdownButton from 'react-bootstrap/DropdownButton'
import { FaSearch,FaCartArrowDown,FaHeart,FaSnapchatGhost,FaShoppingBag } from "react-icons/fa";

import 'bootstrap/dist/css/bootstrap.css'
import {  NavDropdown,Form,Nav,FormControl,} from "react-bootstrap";

import './Header.css'
function Header(){
    return(
  <div class="header-nav">
    <Navbar expand="lg" className='nav' >
      <Navbar.Brand href="#home">E_Shop</Navbar.Brand>
        <Nav className="mr-auto">
          <NavDropdown alignRight title="Catégories" id="basic-nav-dropdown" >
          </NavDropdown>
        </Nav> 
        
        <Form inline>

  
        <InputGroup>

          <FormControl type="text" placeholder=" Search" className="mr-sm-2" />
          <div className='search-icon'><FaSearch /></div>
        </InputGroup>  

          <Button  className='Connexion'variant="outline-success">Connexion</Button>
          <div className="button">  
          <i><FaShoppingBag className="icon"/></i>
          <span> 0 ARTICLES </span></div>
        

        </Form>
      
    </Navbar>
    </div>
    )

}

export default Header;