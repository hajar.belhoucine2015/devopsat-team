import {Form,Button} from 'react-bootstrap';
import { FcGoogle } from "react-icons/fc";
import '../Connexion/LeftSide.css';
function LeftSide(){
    return(
<div className="form-icon">
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Entrer email" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
          </Form.Group>

          <Form.Group controlId="formBasicCheckbox">
            <a href="#">Mot de passe oublié ?</a>
          </Form.Group>
          
            <div className="connexbtn"> <Button className="submit"variant="primary" type="submit">Se Connecter </Button>
            </div>
            <br/>
            <div className="connexbtn"> <Button className="submit"variant="primary" type="submit">< FcGoogle/> Se Connecter avec Google</Button>
            </div>
        
          </Form>
</div>
    )
}
export default LeftSide;
