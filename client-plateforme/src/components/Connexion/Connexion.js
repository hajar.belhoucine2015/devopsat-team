import {Col,Row} from 'react-bootstrap'
import Footer from '../Footer/Footer';
import Menu from '../Header/Menu';
import RightSide from './RightSide';
import LeftSide from './LeftSide';
import '../Connexion/Connexion.css'

function Connexion(){
    return(
    <div> 
      <Menu/>
      <div className="container">
            <Row className="landing">
                <Col><LeftSide /></Col>
             
                <Col><RightSide /></Col>
            </Row>
          </div>
      <Footer/>
  </div> 
    )
}
export default Connexion;