import React from 'react';
import image1 from '../images/img2.jpg';
import image2 from '../images/img1.jpg';
import image3 from '../images/img2.jpg';
function Acceuil(){
    return(
        <div className="acceuil-slide">
            <div className="slide1">
                <img class="slides" src={image1}/>
            </div>

            <div className="slide2">
            <img class="slides" src={image3}/>
            </div>
            
            <div className="slide3  ">
            <img class="slides" src={image2}/>
            </div>

        </div>
    )
}
export default Acceuil;