import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Connexion from './components/Connexion/Connexion';
import Register from './components/Register/Register';
import Produits from './components/Produits/Produits';
import Card from './components/card/Card';


const Root= () =>(
  <Router>
 <Switch>
      <Route exact path="/" component={App} />
      <Route path="/register" component={Register} />
      <Route path="/connexion" component={Connexion} />
      <Route path="/produits" component={Produits} />
      <Route path="/card" component={Card} />

      </Switch>
  </Router>
)
ReactDOM.render(
  <Root/>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
