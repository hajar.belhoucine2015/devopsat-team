import './App.css';
import Footer from './components/Footer/Footer';
import 'bootstrap/dist/css/bootstrap.css'
import Home from './components/Home/Home'
import Menu from './components/Header/Menu'
function App() {
  return (
    <div className="App">
     <Menu/>
     <div id="content-wrap">
  <Home/>
     </div>


     <Footer/>
     
    </div>
  );
}

export default App;
