package com.example.ServiceProduct;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class ServiceProduitApplicationTests {
	//Le MockMvc fournit un support JUnit pour tester un code Spring MVC d’une manière simple.
	@Autowired
	MockMvc mockMvc;

	@Test
	public void  listeProduit() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/listProduit")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
	}
	@Test
	public void liteOneProduit() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/infoProduit/11")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
	}
	/*@Test
	public void addProduit() throws Exception {
		String newRide = "{\"description\":\"descTest\",\"image1_url\":\"image1test\",\"prix_unitaire\":2.5}";
		mockMvc.perform(MockMvcRequestBuilders.post("/ajoutProduit")

				.contentType(MediaType.APPLICATION_JSON)
				.content(newRide)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
	}

	*/


}
