package com.example.ServiceProduct.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table
public class Designation {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id_designation;
    String taille;
    @JsonIgnore
    public Collection<Produit> getProduitst() {
        return produitst;
    }

    public void setProduitst(Collection<Produit> produitst) {
        this.produitst = produitst;
    }

    String couleur;
    String sexe;
    String pointure;
    @OneToMany(mappedBy="designation",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    private Collection<Produit> produitst;

    public Long getId_designation() {
        return id_designation;
    }

    public void setId_designation(Long id_designation) {
        this.id_designation = id_designation;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPointure() {
        return pointure;
    }

    public void setPointure(String pointure) {
        this.pointure = pointure;
    }

    public  Designation(){

    }
}
