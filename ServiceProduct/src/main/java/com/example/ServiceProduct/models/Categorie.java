package com.example.ServiceProduct.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Categorie {
    public Categorie(Long id_categorie, String nom_categorie, String description) {
        this.id_categorie = id_categorie;
        this.nom_categorie = nom_categorie;
        this.description = description;
    }


    public  Categorie(){

    }

    public Long getId_categorie() {
        return id_categorie;
    }

    public void setId_categorie(Long idcategorie) {
        this.id_categorie = idcategorie;
    }

    public String getNom_categorie() {
        return nom_categorie;
    }
    @JsonIgnore
    public Collection<Produit> getProduitst() {
        return produitst;
    }

    public void setProduitst(Collection<Produit> produitst) {
        this.produitst = produitst;
    }

    public void setNom_categorie(String nom_categorie) {
        this.nom_categorie = nom_categorie;
    }

    public String getFamille() {
        return description;
    }

    public void setFamille(String description) {
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
     Long id_categorie;
    String nom_categorie;
    String description;


    @OneToMany(mappedBy="categorie",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    private Collection<Produit> produitst;







}
