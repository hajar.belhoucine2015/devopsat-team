package com.example.ServiceProduct.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Locale;


@Entity
@Table
public class Produit {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id_produit;
    String titre_produit;
    String description;
    Double prix_unitaire;
    String image_originale;
    public Produit() {
        super();

    }
    @ManyToOne
    @JoinColumn(name="id_categorie")
    private Categorie categorie;

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    @ManyToOne
    @JoinColumn(name="id_designation")
    private Designation designation;

    public Produit(Long id_produit, String titre_produit, String description, Double prix_unitaire, String image_originale, Categorie categorie, String image1_url, String image2_url, String image3_url, int qte_stock,Designation designation) {
        this.id_produit = id_produit;
        this.titre_produit = titre_produit;
        this.description = description;
        this.prix_unitaire = prix_unitaire;
        this.image_originale = image_originale;
        this.categorie = categorie;
        this.designation =designation;
        this.image1_url = image1_url;
        this.image2_url = image2_url;
        this.image3_url = image3_url;
        this.qte_stock = qte_stock;
    }

    String image1_url;

    public Long getId_produit() {
        return id_produit;
    }

    public void setId_produit(Long id_produit) {
        this.id_produit = id_produit;
    }

    public String getTitre_produit() {
        return titre_produit;
    }

    public void setTitre_produit(String titre_produit) {
        this.titre_produit = titre_produit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrix_unitaire() {
        return prix_unitaire;
    }

    public void setPrix_unitaire(Double prix_unitaire) {
        this.prix_unitaire = prix_unitaire;
    }

    public String getImage_originale() {
        return image_originale;
    }

    public void setImage_originale(String image_originale) {
        this.image_originale = image_originale;
    }

    public String getImage1_url() {
        return image1_url;
    }

    public void setImage1_url(String image1_url) {
        this.image1_url = image1_url;
    }

    public String getImage2_url() {
        return image2_url;
    }

    public void setImage2_url(String image2_url) {
        this.image2_url = image2_url;
    }

    public String getImage3_url() {
        return image3_url;
    }

    public void setImage3_url(String image3_url) {
        this.image3_url = image3_url;
    }

    public int getQte_stock() {
        return qte_stock;
    }

    public void setQte_stock(int qte_stock) {
        this.qte_stock = qte_stock;
    }

    String image2_url;
    String image3_url;
    int qte_stock;


    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}
