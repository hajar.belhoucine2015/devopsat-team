package com.example.ServiceProduct.interfaceMetierImpl;

import com.example.ServiceProduct.Repository.CategorieRepository;
import com.example.ServiceProduct.Repository.ProduitRepository;
import com.example.ServiceProduct.interfaceMetier.CategorieMetier;
import com.example.ServiceProduct.models.Categorie;
import com.example.ServiceProduct.models.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorieMetierImpl implements CategorieMetier {
    @Autowired
    CategorieRepository categorieRepository;


    @Override
    public Categorie getCategorie(Long id) {
        return categorieRepository.findById(id).get();
    }


    @Override
    public List<Categorie> getCategories() {

        return categorieRepository.findAll();
    }

    @Override
    public List<Categorie> chercher(String mc) {

        return categorieRepository.chercher("%"+mc+"%");
    }

    @Override
    public Categorie saveCategorie(Categorie c) {
        System.out.println("save");
        return categorieRepository.save(c);
    }
    @Override
    public Categorie upDateCategorie(Long id,Categorie c) {
       Categorie categorie=categorieRepository.findById(id).get();
        if(id==categorie.getId_categorie()) {
            c.setId_categorie(id);
        }
        return  categorieRepository.save(c);

    }

}
