package com.example.ServiceProduct.interfaceMetierImpl;

import com.example.ServiceProduct.Repository.ProduitRepository;
import com.example.ServiceProduct.interfaceMetier.ProductMetier;
import com.example.ServiceProduct.models.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProduitMetierImpl implements ProductMetier {


    @Autowired
    ProduitRepository produitRepository;


    @Override
    public Produit getProduit(Long id) {
        return produitRepository.findById(id).get();
    }

    @Override
    public boolean deleteProduct(Long id) {
        produitRepository.deleteById(id);
        return true;
    }
    @Override
    public List<Produit> getProduits() {

       // return produitRepository.findAll().subList(0,2);
        return produitRepository.findAll();
    }

    @Override
    public List<Produit> chercher(String mc) {

        return produitRepository.chercher("%"+mc+"%");
    }

    @Override
    public Produit saveProduit(Produit c) {
        System.out.println("save");
        return produitRepository.save(c);
    }
    @Override
    public Produit upDateProduit(Long id,Produit c) {
        Produit produit=produitRepository.findById(id).get();
        if(id==produit.getId_produit()) {
            c.setId_produit(id);
        }
        return  produitRepository.save(c);

    }

  /*  @Override
    public Produit getProduitByTitre(String titre_produit) {
        return produitRepository.findByTitre_produit(titre_produit);
    }*/








}
