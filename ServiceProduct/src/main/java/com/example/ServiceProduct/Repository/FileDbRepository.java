package com.example.ServiceProduct.Repository;

import com.example.ServiceProduct.models.FileDb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDbRepository extends JpaRepository<FileDb, String> {

}