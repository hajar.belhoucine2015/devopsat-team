package com.example.ServiceProduct.Repository;

import com.example.ServiceProduct.models.Categorie;
import com.example.ServiceProduct.models.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategorieRepository  extends JpaRepository<Categorie,Long> {
    @Query("select c from Categorie c  where c.nom_categorie like :x  ")
    public List<Categorie> chercher(@Param("x") String mc);

}
