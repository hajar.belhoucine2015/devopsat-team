package com.example.ServiceProduct.Repository;

import com.example.ServiceProduct.models.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit,Long> {

  //  Produit findByTitre_produit(String titre_produit);
  @Query("select c from Produit c  where c.titre_produit like :x or c.description like :x ")
  public List<Produit> chercher(@Param("x") String mc);

}

