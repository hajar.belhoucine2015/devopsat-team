package com.example.ServiceProduct.cnx;
import com.example.ServiceProduct.models.Card;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="laravel",url = "http://localhost:8000/api/")
public interface CommandRequest {

    @GetMapping("card")
    List<Card> getAll();

    @GetMapping("card/{id_client}")
    Card getClientCard(@PathVariable int id_client);

    @PostMapping("card")
    Card createCard(@RequestParam Card c);


    @PostMapping("card/{id_client}")
    Card updateCard(@PathVariable int id_client,@RequestParam Card c);

    @DeleteMapping("card/{id_client}")
    boolean deleteCard(@PathVariable int id_client);

}
