package com.example.ServiceProduct.interfaceMetier;

import com.example.ServiceProduct.models.Produit;

import java.util.List;

public interface ProductMetier {

        public Produit getProduit(Long id);
     //   public Produit getProduitByTitre(String titre_produit);
         public boolean deleteProduct(Long id);


        public List<Produit> getProduits();
        public List<Produit> chercher(String mc);
        public Produit saveProduit(Produit c);
         public Produit upDateProduit(Long id,Produit c);







}
