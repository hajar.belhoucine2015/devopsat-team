package com.example.ServiceProduct.interfaceMetier;

import com.example.ServiceProduct.models.Categorie;
import com.example.ServiceProduct.models.Produit;

import java.util.List;

public interface CategorieMetier {
    public Categorie getCategorie(Long id);
    public List<Categorie> getCategories();
    public List<Categorie> chercher(String mc);
    public Categorie saveCategorie(Categorie c);
    public Categorie upDateCategorie(Long id,Categorie c);
}
