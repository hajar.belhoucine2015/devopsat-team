package com.example.ServiceProduct.service;


import com.example.ServiceProduct.Repository.FileDbRepository;
import com.example.ServiceProduct.models.FileDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;


@Service
public class FileStorageService {

    @Autowired
    private FileDbRepository fileDBRepository;

    public FileDb store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        FileDb FileDB = new FileDb(fileName, file.getContentType(), file.getBytes());

        return fileDBRepository.save(FileDB);
    }

    public FileDb getFile(String id) {
        return fileDBRepository.findById(id).get();
    }

    public Stream<FileDb> getAllFiles() {
        return fileDBRepository.findAll().stream();
    }
}