package com.example.ServiceProduct.service;

import com.example.ServiceProduct.interfaceMetierImpl.CategorieMetierImpl;
import com.example.ServiceProduct.models.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

    @RestController
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001"})
    public class CategorieService {

        @Autowired
        CategorieMetierImpl categorieMetier;

        @RequestMapping(value="/infoCategorie/{id}",method= RequestMethod.GET)
        public Categorie getCategorie(@PathVariable Long id) {
            return categorieMetier.getCategorie(id);
        }



        @RequestMapping(value="/listCategorie",method=RequestMethod.GET)
        public List<Categorie> listeCategorie() {
            System.out.println("list");
            return categorieMetier.getCategories();
        }

        @RequestMapping(value="/chercherCategorie/{mc}",method=RequestMethod.GET)
        public List<Categorie> chercher(@PathVariable String mc){
            System.out.println("cherche");
            return categorieMetier.chercher(mc);}


        @RequestMapping(value="/ajoutCategorie",method=RequestMethod.POST)
        public Categorie saveCategorie(@RequestBody Categorie categorie) {
            System.out.println("ajoutC-1");
            return categorieMetier.saveCategorie(categorie);
        }
        @RequestMapping(value="/updateCategorie/{id}",method=RequestMethod.PUT)
        public Categorie update(@PathVariable long id,@RequestBody Categorie categorie){
            System.out.println("edit");
            categorie.setId_categorie(id);
            return categorieMetier.upDateCategorie(id,categorie);
        }
}
