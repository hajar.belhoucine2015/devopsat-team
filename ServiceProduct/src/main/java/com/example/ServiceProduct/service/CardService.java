package com.example.ServiceProduct.service;

import com.example.ServiceProduct.cnx.CommandRequest;
import com.example.ServiceProduct.models.Card;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class CardService {

     CommandRequest cmd;

    @RequestMapping(value="/card",method= RequestMethod.GET)
    public List<Card> getCards() {
        return cmd.getAll();
    }
}
