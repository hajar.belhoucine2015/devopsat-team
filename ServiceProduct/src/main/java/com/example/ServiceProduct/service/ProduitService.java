package com.example.ServiceProduct.service;

import com.example.ServiceProduct.interfaceMetierImpl.ProduitMetierImpl;
import com.example.ServiceProduct.models.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001"})

public class ProduitService {

    @Autowired
    ProduitMetierImpl produitMetier;

    @RequestMapping(value="/infoProduit/{id}",method= RequestMethod.GET)
    public Produit getProduit(@PathVariable Long id) {
        return produitMetier.getProduit(id);
    }


    @RequestMapping(value="/deleteProduit/{id}",method=RequestMethod.DELETE)
    public boolean supprimer(@PathVariable long id){

        return produitMetier.deleteProduct(id);
    }
    @RequestMapping(value="/listProduit",method=RequestMethod.GET)
    public List<Produit> listeProduit() {
        System.out.println("list");
        return produitMetier.getProduits();
    }

    @RequestMapping(value="/chercherProduit/{mc}",method=RequestMethod.GET)
    public List<Produit> chercher(@PathVariable String mc){
        System.out.println("cherche");
        return produitMetier.chercher(mc);}


    @RequestMapping(value="/ajoutProduit",method=RequestMethod.POST)
    public Produit saveProduit(@RequestBody Produit produit) {
        System.out.println("ajoutP-1");
        return produitMetier.saveProduit(produit);
    }
    @RequestMapping(value="/updateProduit/{id}",method=RequestMethod.PUT)
    public Produit update(@PathVariable long id,@RequestBody Produit produit){
        System.out.println("edit");
        produit.setId_produit(id);
        return produitMetier.upDateProduit(id,produit);
    }

}
