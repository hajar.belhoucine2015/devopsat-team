<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCard;
use Illuminate\Http\Request;

class ShoppingCardController extends Controller
{
    public function index()
    {
        return ShoppingCard::all();
    }

    public function show($id)
    {
        return ShoppingCard::findOrFail($id);
    }


    public function create(Request $request)
    {
        $shopping_card = new ShoppingCard();
        $shopping_card->id_product = $request->input('id_product');
        $shopping_card->id_client = $request->input('id_client');
        $shopping_card->prix = $request->input('prix');
        $shopping_card->qte = $request->input('qte');
         $shopping_card->save();
        return $shopping_card;

    }
    public function update(Request $request,$id)
    {
        $shopping_card =  ShoppingCard::findOrFail($id);
        $shopping_card->id_product = $request->input('id_product');
        $shopping_card->id_client = $request->input('id_client');
        $shopping_card->prix = $request->input('prix');
        $shopping_card->qte = $request->input('qte');
        $shopping_card->save();
        return $shopping_card;

    }
    public function delete($id)
    {
        $shopping_card = ShoppingCard::findOrFail($id);
        $shopping_card->delete();
        return true;
    }
}
