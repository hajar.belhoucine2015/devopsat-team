<?php

use App\Http\Controllers\ShoppingCardController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('card', [ShoppingCardController::class, 'index']);
Route::get('card/{id}', [ShoppingCardController::class, 'show']);
Route::post('card', [ShoppingCardController::class, 'create']);
Route::post('card/{id}', [ShoppingCardController::class, 'update']);
Route::delete('card/{id}', [ShoppingCardController::class, 'delete']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
